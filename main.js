// const ray = {
//     name: "ray",
//     age: 26,
//     approvedCourses: ["Curso 1"],
//     addCourse(newCourse) {
//         this.approvedCourses.push(newCourse);
//     },
// };

// Object.keys(ray);
// Object.getOwnPropertyNames(ray);
// Object.entries(ray);

// Object.defineProperty(ray, 'navigator', {
//     value: 'Chrome',
//     writable: true,
//     enumerable: false,
//     configurable: true,
// });

// Object.defineProperty(ray, 'editor', {
//     value: 'VSCode',
//     writable: false,
//     enumerable: true,
//     configurable: true,
// });

// Object.defineProperty(ray, 'terminal', {
//     value: 'WSL',
//     writable: true,
//     enumerable: true,
//     configurable: false,
// });

// Object.defineProperty(ray, 'pruebaNasa', {
//     value: 'extraterrestres',
//     writable: false, // no editar
//     enumerable: false, //ocultar
//     configurable: false, //no borrar
// });

// Object.seal(ray); //evita que las propiedades se puedan borrar
// Object.freeze(ray); //evita que las propiedades se puedan borrar y editar

// console.log(Object.getOwnPropertyDescriptors(ray));

// const obj1 = {
//     a: 'a',
//     b: 'b',
//     c: {
//         d: 'd',
//         e: 'e',
//     },
// };

// // const obj2 = {};
// // for (const prop in obj1) {
// //     obj2[prop] = obj1[prop];
// // }

// // const obj3 = Object.assign({}, obj1);
// // const obj4 = Object.create(obj1);

// const stringifyComplexObj = JSON.stringify(obj1);
// const obj2 = JSON.parse(stringifyComplexObj);
// console.log(obj2);

// function recursiva(parametro) {
//     if()
// }
// const numeros = [0, 1, 24, 5, 7, 8, 5, 2, 7];
// let numero = 0;
// for (let index = 0; index < numeros.length; index++) {
//     numero = numeros[index];
//     console.log(index, numero);
// }

// function recursiva(numbersArray) {
//     if (numbersArray.length != 0) {
//         const firstNum = numbersArray[0];
//         console.log(firstNum);
//         numbersArray.shift();
//         recursiva(numbersArray);
//     }
// }

// recursiva(numeros);

// const obj1 = {
//     a: 'a',
//     b: 'b',
//     c: {
//         d: 'd',
//         e: 'e',
//     },
//     editA() {
//         this.a = 'AAAAAAAAA';
//     },
// };

function isObject(subject) {
    return typeof subject == "object";
}

function isArray(subject) {
    return Array.isArray(subject);
}

function deepCopy(subject) {
    let copySubject;

    const subjectIsArray = isArray(subject);
    const subjectIsObject = isObject(subject);

    if (subjectIsArray) {
        copySubject = [];
    } else if (subjectIsObject) {
        copySubject = {};
    } else {
        return subject;
    }

    for (const key in subject) {
        const keyIsObject = isObject(subject[key]);

        if (keyIsObject) {
            copySubject[key] = deepCopy(subject[key]);
        } else {
            if (subjectIsArray) {
                copySubject.push(subject[key]);
            } else {
                copySubject[key] = subject[key];
            }
        }
    }

    return copySubject;
}

// class SuperObject {
//     static isArray(subject) {
//         return Array.isArray(subject);
//     }

//     static isObject(subject) {
//         return typeof subject == "object";
//     }

//     static deepCopy(subject) {
//         let copySubject;

//         const subjectIsArray = isArray(subject);
//         const subjectIsObject = isObject(subject);

//         if (subjectIsArray) {
//             copySubject = [];
//         } else if (subjectIsObject) {
//             copySubject = {};
//         } else {
//             return subject;
//         }

//         for (const key in subject) {
//             const keyIsObject = isObject(subject[key]);

//             if (keyIsObject) {
//                 copySubject[key] = deepCopy(subject[key]);
//             } else {
//                 if (subjectIsArray) {
//                     copySubject.push(subject[key]);
//                 } else {
//                     copySubject[key] = subject[key];
//                 }
//             }
//         }

//         return copySubject;
//     }
// }

function SuperObject() {}
SuperObject.isObject = function (subject) {
    return typeof subject == "object";
};
SuperObject.deepCopy = function (subject) {
    let copySubject;

    const subjectIsArray = isArray(subject);
    const subjectIsObject = isObject(subject);

    if (subjectIsArray) {
        copySubject = [];
    } else if (subjectIsObject) {
        copySubject = {};
    } else {
        return subject;
    }

    for (const key in subject) {
        const keyIsObject = isObject(subject[key]);

        if (keyIsObject) {
            copySubject[key] = deepCopy(subject[key]);
        } else {
            if (subjectIsArray) {
                copySubject.push(subject[key]);
            } else {
                copySubject[key] = subject[key];
            }
        }
    }

    return copySubject;
};

// const obj2 = deepCopy(obj1);
// console.log(obj2);

// const studentBase = {
//     name: undefined,
//     email: undefined,
//     age: undefined,
//     approvedCourses: undefined,
//     learningPaths: undefined,
//     socialMedia: {
//         twitter: undefined,
//         inmstagram: undefined,
//         facebook: undefined,
//     },
// };

// const ray2 = deepCopy(studentBase);
// Object.seal(ray2);
// console.log(Object.isSealed(ray2)); //Comprueba si las propiedades del objeto no se pueden eliminar
// console.log(Object.isFrozen(ray2)); //Comprueba si las propiedades del objeto no se pueden editar
// // ray2.name = "Ray";
// // Object.defineProperty(ray2, "name", {
// //     value: "Ray",
// //     configurable: false,
// // });

function requiredParam(param) {
    throw new Error(`${param} es obligatorio`);
}

function LearningPath({ name = requiredParam("name"), courses = [] }) {
    this.name = name;
    this.courses = courses;

    const private = {
        _name: name,
        _courses: courses,
    };

    // const public = {
    //     get name() {
    //         return private["_name"];
    //     },

    //     set name(newName) {
    //         if (newName.length != 0) {
    //             private["_name"] = newName;
    //         } else {
    //             console.warn("Tu nombre debe tener al menos 1 caracter");
    //         }
    //     },

    //     get courses() {
    //         return private["_courses"];
    //     },
    // };

    // return public;
}

function Student({
    name = requiredParam("name"),
    age,
    email = requiredParam("email"),
    twitter,
    instagram,
    facebook,
    approvedCourses = [],
    learningPaths: [],
} = {}) {
    this.name = name;
    this.age = age;
    this.email = email;
    this.socialMedia = {
        twitter,
        instagram,
        facebook,
    };
    this.approvedCourses = approvedCourses;
    this.learningPaths = learningPaths;

    const private = {
        _learningPaths: [],
    };

    Object.defineProperty(this, "learningPaths", {
        get() {
            return private["_learningPaths"];
        },

        set(newLP) {
            if (newLP instanceof LearningPath) {
                private["_learningPaths"].push(newLP);
            } else {
                console.warn(
                    "Alguno de los LPs no es una instancia del prototipo LearningPath"
                );
            }
        },

        configurable: false,
    });

    for (learningPathIndex in learningPaths) {
        this.learningPaths = learningPaths[learningPathIndex];
    }

    // const private = {
    //     _name: name,
    //     _learningPaths: learningPaths,
    // };
    // const public = {
    //     age,
    //     email,
    //     socialMedia: {
    //         twitter,
    //         instagram,
    //         facebook,
    //     },
    //     approvedCourses,
    //     get name() {
    //         return private["_name"];
    //     },
    //     set name(newName) {
    //         if (newName.length != 0) {
    //             private["_name"] = newName;
    //         } else {
    //             console.warn("Tu nombre debe tener al menos 1 caracter");
    //         }
    //     },
    //     get learningPaths() {
    //         return private["_learningPaths"];
    //     },
    //     set learningPaths(newLP) {
    //         if (!newLP.name) {
    //             return console.warn("Tu LP no tiene la propiedad name");
    //         }
    //         if (!newLP.courses) {
    //             return console.warn("Tu LP no tiene la propiedad courses");
    //         }
    //         if (!isArray(newLP.courses)) {
    //             return console.warn("Tu LP no tiene courses");
    //         }
    //         private["_learningPaths"].push(newLP);
    //     },
    // readName() {
    //     return private["_name"];
    // },
    // changeName(newName) {
    //     private["_name"] = newName;
    // },
    // };
    // Object.defineProperty(public, "readName", {
    //     configurable: false,
    //     writable: false,
    // });
    // Object.defineProperty(public, "changeName", {
    //     configurable: false,
    //     writable: false,
    // });
    // return public;
}

const escuelaWeb = new LearningPath({ name: "Escuela de WebDev" });
const escuelaDate = new LearningPath({ name: "Escuela de DateScience" });
const ray = new Student({
    name: "Ray",
    email: "blondell2811@gmail.com",
    learningPaths: [esculaWeb, escuelaData],
});

console.log(ray);
